(in-package :heart-rate)

(qrequire :quick)

(defun run ()
  (when (ensure-android-permission "android.permission.BODY_SENSORS")
    (ini-sensors))
  (qlater 'update-heart-rate))

;; initially, a demo mode will be running; only when real data starts to
;; arrive, the border will turn green and real sensor data will be shown

(defun round* (n)
  (truncate (+ 0.5 n)))

(let ((demo-mode t))
  (defun update-heart-rate ()
    (q! |stop| ui:*animation*)
    (let ((bpm (heart-rate))
          (iv 1000))
      (when (and bpm (not (zerop bpm)))
        (when demo-mode
          (setf demo-mode nil)
          (q> |border.color| ui:*main* "#40c040"))
        (let ((ms (round* (/ 60000 bpm 2))))
          (q> |duration| ui:*zoom-in*  ms)
          (q> |duration| ui:*zoom-out* ms)
          (q> |text| ui:*heart-rate* (princ-to-string bpm))
          (q> |text| ui:*accuracy*   (princ-to-string (heart-rate-accuracy)))
          (setf iv (* 2 ms))))
      (q! |start| ui:*animation*)
      (qsingle-shot iv 'update-heart-rate))))
