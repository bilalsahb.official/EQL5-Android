;;; THIS FILE IS GENERATED, see '(eql:qml)'

(defpackage ui
  (:use :cl :eql)
  (:export
   #:*file-browser*
   #:*flick-output*
   #:*folder-model*
   #:*folder-view*
   #:*history-back*
   #:*history-forward*
   #:*main*
   #:*path*
   #:*progress*
   #:*repl-container*
   #:*repl-input*
   #:*repl-output*))

(provide :ui-vars)

(in-package :ui)

(defparameter *history-back*    "history_back")    ; Button          "qml/ext/Repl.qml"
(defparameter *history-forward* "history_forward") ; Button          "qml/ext/Repl.qml"
(defparameter *repl-container*  "repl_container")  ; Column          "qml/ext/Repl.qml"
(defparameter *file-browser*    "file_browser")    ; FileBrowser     "qml/ext/FileBrowser.qml"
(defparameter *flick-output*    "flick_output")    ; Flickable       "qml/ext/Repl.qml"
(defparameter *folder-model*    "folder_model")    ; FolderListModel "qml/ext/FileBrowser.qml"
(defparameter *folder-view*     "folder_view")     ; ListView        "qml/ext/FileBrowser.qml"
(defparameter *progress*        "progress")        ; ProgressBar     "qml/ext/Repl.qml"
(defparameter *main*            "main")            ; StackView       "qml/my.qml"
(defparameter *repl-output*     "repl_output")     ; TextEdit        "qml/ext/Repl.qml"
(defparameter *path*            "path")            ; TextField       "qml/ext/FileBrowser.qml"
(defparameter *repl-input*      "repl_input")      ; TextField       "qml/ext/Repl.qml"
