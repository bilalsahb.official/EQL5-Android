;;; for developing apps using the 'CL myApp' APK, without installing dev tools;
;;; this file will be loaded on startup

(in-package :eql)

(defun ui ()
  (let ((qml "qml/my.qml"))
    (when (probe-file qml)
      (|clearComponentCache| (|engine| qml:*quick-view*))
      (|setSource| qml:*quick-view* (|fromLocalFile.QUrl| qml)))))

(export 'ui)

(progn
  (ui)
  (x:when-it (probe-file "lisp/ui-vars")
    (load x:it)))

;; this will start Swank in the background every time you restart the app
;;(defvar %once-only% (mp:process-run-function :swank 'start-swank))
