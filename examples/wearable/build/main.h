#ifndef MAIN_H
#define MAIN_H

#include <QtAndroid> 
#include <QAndroidJniEnvironment>
#include <QApplication>
#include <QtDebug>
#include <qevent.h> // for undocumented 'QApplicationStateChangeEvent'

QT_BEGIN_NAMESPACE

class CBridge : public QObject {
    Q_OBJECT
public:
    CBridge(QObject* parent, const QString& name) : QObject(parent) {
        setObjectName(name); }

    Q_INVOKABLE void keepScreenOn() {
        QtAndroid::runOnAndroidThread([]{
            QAndroidJniObject activity = QtAndroid::androidActivity();
            if(activity.isValid()) {
                QAndroidJniObject window = activity.callObjectMethod("getWindow", "()Landroid/view/Window;");
                if(window.isValid()) {
                    const int FLAG_KEEP_SCREEN_ON = 128;
                    window.callMethod<void>("addFlags", "(I)V", FLAG_KEEP_SCREEN_ON); }}
            QAndroidJniEnvironment env;
            if(env->ExceptionCheck()) {
                env->ExceptionClear(); }}); }

    Q_INVOKABLE bool checkPermission(const QString& name) {
        // e.g. "android.permission.WRITE_EXTERNAL_STORAGE"
        // this handles the new Android permission model, starting with API version 23 (Android 6),
        // that is: we need to ask for the permission at runtime (every time we need a permission)
#if QT_VERSION > 0x050A00 // 5.10
        QtAndroid::PermissionResult res = QtAndroid::checkPermission(name);
        if(res == QtAndroid::PermissionResult::Denied) {
            QtAndroid::requestPermissionsSync(QStringList() << name);
            res = QtAndroid::checkPermission(name);
            if(res == QtAndroid::PermissionResult::Denied) {
                return false; }}
#endif
        return true; }
};

class EventApplication : public QApplication {
    Q_OBJECT
public:
    EventApplication(int& argc, char** argv) : QApplication(argc, argv) {
        installEventFilter(this); }

    bool eventFilter(QObject* object, QEvent* event) override {
        // hack to quit app when swiped to the right; if a timer event happens
        // right after a state change to suspended, we can assume that it is in
        // the process of quitting, so we force-quit it immediately
        static bool suspended = false;
        QEvent::Type type = event->type();
        if(type == QEvent::ApplicationStateChange) {
            int state = ((QApplicationStateChangeEvent*)event)->applicationState();
            if(state == Qt::ApplicationSuspended) {
                suspended = true; }
            else if(state == Qt::ApplicationActive) {
                suspended = false; }}
        else if(type == QEvent::Timer) {
            if(suspended) {
                quitApp(); }}
        return QApplication::eventFilter(object, event); }

Q_SIGNALS:
    void quitApp();
};

QT_END_NAMESPACE

#endif
