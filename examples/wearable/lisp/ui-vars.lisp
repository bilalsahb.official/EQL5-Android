;;; THIS FILE IS GENERATED, see '(eql:qml)'

(defpackage ui
  (:use :cl :eql)
  (:export
   #:*blah*
   #:*echo*
   #:*echo-anim*
   #:*float*
   #:*real-den*
   #:*real-num*
   #:*remote-ip*
   #:*swank*))

(provide :ui-vars)

(in-package :ui)

(defparameter *blah*      "blah")      ; Button          "qml/wearable.qml"
(defparameter *echo-anim* "echo_anim") ; NumberAnimation "qml/wearable.qml"
(defparameter *swank*     "swank")     ; RoundButton     "qml/wearable.qml"
(defparameter *echo*      "echo")      ; Text            "qml/wearable.qml"
(defparameter *float*     "float")     ; Text            "qml/wearable.qml"
(defparameter *real-den*  "real_den")  ; Text            "qml/wearable.qml"
(defparameter *real-num*  "real_num")  ; Text            "qml/wearable.qml"
(defparameter *remote-ip* "remote_ip") ; Tumbler         "qml/wearable.qml"
