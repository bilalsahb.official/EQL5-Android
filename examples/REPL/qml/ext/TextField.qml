import QtQuick 2.10
import QtQuick.Controls 2.10

TextField {
  palette {
    highlight: "#007aff"
    highlightedText: "white"
  }
}
