;;; For NDK versions below 19, you need to create a standalone toolchain.

(in-package :cl-user)

(load "lisp/x.lisp")

(pushnew :android *features*)
(pushnew :aarch64 *features*)

(require :cmp)

(defvar *ndk-toolchain* (ext:getenv "ANDROID_NDK_TOOLCHAIN"))
(defvar *ecl-android*   (ext:getenv "ECL_ANDROID"))

(setf compiler::*ecl-include-directory* (x:cc *ecl-android* "/include/")
      compiler::*ecl-library-directory* (x:cc *ecl-android* "/lib/"))

(setf compiler::*cc*       (let ((path (or (probe-file (x:cc *ndk-toolchain* "/bin/aarch64-linux-android21-clang")) ; >= ndk-19
                                           (probe-file (x:cc *ndk-toolchain* "/bin/aarch64-linux-android-clang"))   ; <= ndk-18
                                           (error "clang compiler not found"))))
                                  (namestring path))
      compiler::*ld*       (x:cc *ndk-toolchain* "/bin/aarch64-linux-android-ld")
      compiler::*ar*       (x:cc *ndk-toolchain* "/bin/aarch64-linux-android-ar")
      compiler::*ranlib*   (x:cc *ndk-toolchain* "/bin/aarch64-linux-android-ranlib")
      compiler::*cc-flags* (x:join (list "-DANDROID -DPLATFORM_ANDROID"
                                         "-O2 -fPIC -fno-common -D_THREAD_SAFE"
                                         (x:cc "-I" *ecl-android* "/build/gmp"))))

(format t "~%*** cross compiling for 'aarch64' ***~%")
